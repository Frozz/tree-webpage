// react router
import React from 'react';
import LandingRouter from './LandingRouter';
import './styles/LandingPage.css';

class LandingPage extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div id='landingpage' className='landingpage'>
        <div className='app-wrapper app-wrapper__landing'>
          <LandingRouter />
        </div>
      </div>
    )
  }
}

export default LandingPage;