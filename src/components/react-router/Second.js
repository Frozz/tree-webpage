import React from 'react';
import susza from '../../images/susza.svg';


const Second = () => {
  return (
    <div> 
      <div className='landingpage-text'>
        <h1>Susza</h1>
        <p>Susza powoduje przesuszenie gleby, zmniejszenie lub całkowite zniszczenie upraw roślin alimentacyjnych
        (a co za tym idzie klęski głodu), zmniejszenie zasobów wody pitnej, a także zwiększone prawdopodobieństwo
        katastrofalnych pożarów.
          </p>
      </div>
      <div>
        <img src={susza} />
      </div>
    </div>
  )
}

export default Second;