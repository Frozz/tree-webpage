import React from 'react';
import fabric from '../../images/fabric.svg';


const Fourth = () => {
  return (
    <div>
      <div className='landingpage-text'>
        <h1>Zanieczyszczenia powietrza</h1>
        <p>Zanieczyszczenia powietrza są głównymi przyczynami globalnych zagrożeń środowiska.
        Spalanie paliw kopalnych, działalność rolnicza, eksploatacja górnicza to tylko niektóre z
        głównych przyczyn zanieczyszczenia powietrza.
          </p>
      </div>
      <div>
        <img src={fabric} />
      </div>
    </div>
  )
}

export default Fourth;