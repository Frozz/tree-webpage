import React from 'react';
import wycinka from '../../images/wycinka.svg';
import '../styles/First.css';

const First = () => {
  return (
    <div>
      <div className='landingpage-text'>
        <h1>Wycinanie drzew</h1>
        <p>Proces zmniejszania udziału terenów leśnych w ogólnej powierzchni danego obszaru,
        zazwyczaj wskutek antropopresji (np. nadmierne wykorzystanie gospodarcze lasów, zanieczyszczenie
        środowiska itp.). 
          </p>
      </div>
      <div>
        <img src={wycinka} />
      </div>
    </div>

  )
}

export default First;