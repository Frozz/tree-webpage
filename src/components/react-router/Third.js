import React from 'react';
import fire from '../../images/fire.svg';


const Third = () => {
  return (
    <div>
      <div className='landingpage-text'>
        <h1>Pożary</h1>
        <p>Pożary są jednym ze zjawisk zachodzących w lasach.
        Z punktu widzenia gospodarki leśnej zaliczane są do najpoważniejszych niebezpieczeństw zagrażających lasom.
        Corocznie na świecie notuje się blisko 2000 pożarów.
         </p>
      </div>
      <div>
        <img src={fire} />
      </div>
    </div>
  )
}

export default Third;