import React from 'react';
import map from '../images/map.svg';
import './styles/Map.css';

const Map = () => {
  return (
    <div id='map' className='map'>
      <div className='app-wrapper'>
        <h1>Poziom zalesienia świata</h1>
        <img src={map}></img>
      </div>
    </div>
  )
}

export default Map;