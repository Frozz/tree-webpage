// inny styl dla follow
import React from 'react';
import logo from '../images/logo.png';
import './styles/Nav.css';

class Nav extends React.Component {
  constructor() {
    super();

    this.state = {
      navColor: '',
    }
  }

  componentDidMount() {
    document.addEventListener("scroll", () => {
      const backgroundcolor = window.scrollY < 180 ? "white" : `#303030`;
    

      this.setState({ navColor: backgroundcolor });
    });
  }

  render() {

    return (
      <div id='nav' className='nav' style={{backgroundColor: `${this.state.navColor}`}}>
        <div className='app-wrapper nav-wrapper'>
          <div className='logo'>
            <img src={logo} />
          </div>
          <div className='nav-links'>
            <a href='#landingpage'>Start</a>
            <a href='#knowledge'>O nas</a>
            <a href='#map'>Mapa</a>
            <a href='#about'>Co dalej</a>
            <button id='nav-btn' className='nav-btn'>Oferta</button>
          </div>

          <i className="fas fa-bars nav-menu-icon"></i>

        </div>
      </div>
    )
  }
}

export default Nav;



