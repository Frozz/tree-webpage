import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import First from './react-router/First';
import Second from './react-router/Second';
import Third from './react-router/Third';
import Fourth from './react-router/Fourth';
import './styles/LandingRouter.css';


const LandingRouter = () => {
  return (
    <BrowserRouter>

      <Switch>
        <Route exact path='/' component={First} />
        <Route exact path='/second' component={Second} />
        <Route exact path='/third' component={Third} />
        <Route exact path='/fourth' component={Fourth} />
      </Switch>

      <div className='router-circles landingpage-text'>
        <Link to='/'>
          <div className='router-circle'></div>
        </Link>
        <Link to='/second'>
          <div className='router-circle'></div>
        </Link>
        <Link to='/third'>
          <div className='router-circle'></div>
        </Link>
        <Link to='/fourth'>
          <div className='router-circle'></div>
        </Link>
      </div>

    </BrowserRouter>
  )
}

export default LandingRouter;