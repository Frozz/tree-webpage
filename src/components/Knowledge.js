import React from 'react';
import './styles/Knowledge.css';

const Knowledge = () => {
  return (
    <div id='knowledge' className='knowledge'>
      <div className='app-wrapper app-wrapper__knowledge'>
        <div className='knowledge-text'>
          <h1>Planting Tree - fundacja z pasją do zieleni</h1>
          <span>Dzień Drzewa (ang. Arbor Day) – międzynarodowa akcja sadzenia drzew odbywająca się w
          coraz większej liczbie krajów na świecie. Jej celem jest wzrost świadomości ekologicznej społeczeństw.
          Inicjatorem Dnia Drzewa był Amerykanin Julius Sterling Morton, sekretarz rolnictwa Stanów Zjednoczonych,
          gubernator stanu Nebraska, znawca i miłośnik przyrody. W 1872 r., mieszkając w Nebraska City, zwrócił się
          z apelem do rodaków by 10 kwietnia kto może posadził drzewo. Argumentował, iż „inne święta służą jedynie
          przypomnieniu, dzień drzewa wskazuje zaś na przyszłość”. Idea jego spotkała się z szerokim odzewem i tego dnia
          w USA zasadzono ponad milion drzew. I tak, od tamtej pory, co roku, w drugą niedzielę kwietnia w USA świętowany
          jest Arbor Day.
          </span>
        </div>
        <div className='knowledge-fields'>
          <div className='knowledge-single-field'>
            <div className='text'>Jesteśmy fundacją działającą od pokoleń.
            Istniejemy już 10 lat. Naszym celem jest zalesianie Ziemii tak tylko,
            jak jest to możliwe.
            Wspólnie na przestrzeni lat zasadziliśmy 300.000 drzew.
            </div>
          </div>
          <div className='knowledge-single-field'></div>
          <div className='knowledge-single-field'></div>
          <div className='knowledge-single-field'></div>
          <div className='knowledge-single-field'></div>
          <div className='knowledge-single-field'></div>
        </div>
      </div>
    </div>
  )
}

export default Knowledge;