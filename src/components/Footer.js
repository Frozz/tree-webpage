import React from 'react';
import './styles/Footer.css';

const Footer = () => {
  return (
    <div className='footer'>
      <div className='app-wrapper app-wrapper__footer'>
        <div className='footer-icons'>
          <a href='#'>
            <i className="fab fa-facebook"></i>
          </a>
          <a href='#'>
            <i className="fab fa-instagram"></i>

          </a>
          <a href='#'>
            <i className="fab fa-twitter"></i>
          </a>
          <a href='#'>
            <i className="fab fa-reddit-alien"></i>
          </a>
        </div>
        <span className='footer-text'>PlantingTree - w trosce o drzewa i zieloną przyszłość </span>
        <span className='footer-text'>Tekst: Wikipedia || Projekt: Ł.Płocieniak W.Kozica</span>
        <span className='footer-text contact'>Contact: 000 111 222</span>
      </div>
    </div>
  )
}

export default Footer;