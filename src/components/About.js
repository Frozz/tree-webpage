import React from 'react';
import forest from '../images/forest.svg';
import './styles/About.css';

const About = () => {
  return (
    <div id='about' className='about'>
      <div className='app-wrapper'>
        <div className='about-text'>
          <h1>What's next?</h1>
          <p>Planting trees is a measurable and cost-effective way to reduce carbon emissions,
          because trees are excellent CO2 sinks from the atmosphere. In this way they alleviate the
          greenhouse effect by remaining an important element of regulating the Earth's climate. Trees
          are also a natural barrier limiting the spread of air pollution, and by reducing soil erosion,
          they reduce water pollution. Planting trees along roads reduces noise around busy routes. The
             trees serve as wind and snow barriers and restrict light access.</p>
        </div>
        <img src={forest} />
      </div>
    </div>
  )
}

export default About;