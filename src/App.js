import React from 'react';
import logo from './logo.svg';
import Nav from './components/Nav';
import LandingPage from './components/LandingPage';
import About from './components/About';
import Map from './components/Map';
import Knowledge from './components/Knowledge';
import Footer from './components/Footer';
import './App.css';

const App = () => (
  <div className='app'>
    <Nav />
    <LandingPage />
    <Knowledge />
    <Map />
    <About />
    <Footer />
  </div>
)

export default App;
